# mmp_interactive

Marine metagenomics portal - Interactive explorer of numeric metadata

## Usage

The Docker image is automatically built after each commit.  
**Note:** The version is defined in [.gitlab-ci.yml](.gitlab-ci.yml).

To run the container:  
```
docker pull registry.gitlab.com/uit-sfb/mmp_interactive/mmp-visualization:<version> && \
docker run -p 5006:5006 registry.gitlab.com/uit-sfb/mmp_interactive/mmp-visualization:<version> [options]
```

To access the UI, open the following URL in your web browser:  
`http://localhost:5006`
