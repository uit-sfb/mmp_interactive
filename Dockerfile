FROM continuumio/miniconda3

ENV HOME /root
WORKDIR ${HOME}

RUN apt update && \
  apt install -y curl build-essential
RUN conda config --add channels conda-forge && \
  conda config --add channels bioconda && \
  conda config --add channels bokeh

ENV PATH /opt/conda/envs/env/bin:$PATH

RUN conda install -c conda-forge mamba

RUN mamba create -n env python=3 htseq numpy pandas bokeh=2 && \
  echo "source activate env" > ~/.bashrc

COPY . mmp_interactive

RUN mkdir -p mmp_interactive/tsv

RUN curl -Ls -f "https://databasesapi.sfb.uit.no/rest/v1/MarRef/records?format=tsv&page%5Bsize%5D=-1" > mmp_interactive/tsv/MarRef.tsv

RUN curl -Ls -f "https://databasesapi.sfb.uit.no/rest/v1/MarDB/records?format=tsv&page%5Bsize%5D=-1" > mmp_interactive/tsv/MarDB.tsv

RUN  python mmp_interactive/update_and_deploy_input_data.py

RUN cat mmp_interactive/inputdata/input.tsv | head -n 5

ENTRYPOINT ["bokeh", "serve", "mmp_interactive/"]
